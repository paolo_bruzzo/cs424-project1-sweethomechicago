function Class() {}

Class.prototype.construct = function () {};

Class.extend = function (def) {
    var classDef = function () {
        if (arguments[0] !== Class) {
            this.construct.apply(this, arguments);
        }
    };

    var proto = new this(Class);
    var superClass = this.prototype;

    for (var n in def) {
        var item = def[n];
        if (item instanceof Function) item.superClass = superClass;
        proto[n] = item;
    }

    classDef.prototype = proto;

    //Give this new class the same static extend method    
    classDef.extend = this.extend;
    return classDef;
};

////////////////////////////////////////

// code adapted from http://bl.ocks.org/mbostock/3202354

var barchart_App = Class.extend({

    construct: function () {

        this.scaleFactor = 1;

        this.gender = "people";
        this.age = "all";
        this.race = "White";
        this.selected = [];
        this.areas = [];

        this.margin = {
            top: 40,
            right: 50,
            bottom: 100,
            left: 130
        };
        this.vizWidth = 0;
        this.vizHeight = 0;

        this.canvasWidth = 635 * 4;
        this.canvasHeight = 367 * 4;

        this.svg = null;
        this.myTag = "";
    },

    /////////////////////////////////////////////////////////////

    initApp: function () {
        this.inDataCallbackFunc = this.inDataCallback.bind(this);
    },

    /*
    initAppWithRoomandYear: function(roomNumber, yearNumber)
    {
      this.room = +roomNumber; // number
      this.year = yearNumber.toString(); // string
      this.inDataCallbackFunc = this.inDataCallback.bind(this);
    },
    */

    ////////////////////////////////////////

    startup: function (whereToRender) {
        this.myTag = whereToRender;
        this.svg = d3.select(this.myTag).append("svg");
        this.updateScreen();
    },

    /////////////////////////////////////////////////////////////

    updateWindow: function () {
        var xWin, yWin;

        xWin = d3.select(this.myTag).style("width");
        yWin = d3.select(this.myTag).style("height");

        this.vizWidth = xWin;
        this.vizHeight = yWin;

        var totalSizeX = this.canvasWidth + this.margin.left + this.margin.right;
        var totalSizeY = this.canvasHeight + this.margin.top + this.margin.bottom;

        this.svg = d3.select(this.myTag).select("svg")
            .attr("width", this.vizWidth)
            .attr("height", this.vizHeight)
            .attr("viewBox", "0 0 " + totalSizeX + " " + this.canvasHeight)
        //.attr("viewBox", "" + -this.margin.left + " 0 " + totalSizeX + " " + this.canvasHeight)
        .attr("preserveAspectRatio", "xMinYMin meet");

        //this.svg = d3.select("#viz").select("svg").select("g")
        //.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
    },


    /////////////////////////////////////////////////////////////

    inDataCallback: function (tempData) {
        this.svg.selectAll("g").remove();
        this.svg.selectAll("rect").remove();

        var svg = this.svg;
        var race = this.race;
        var gender = this.gender;
        if (typeof this.age == 'undefined')
            this.age = "all";
        var age = this.age;
        var selected = this.selected;
        var areas = this.areas;

        var color = null;
        switch (race) {
        case "White":
            color = "#dff5ff";
            break;
        case "Black":
            color = "#120505";
            break;
        case "Asian":
            color = "#cea437";
            break;
        case "Other":
            color = "#9f2200";
            break;
        case "Multiracial":
            color = "#005ca7";
            break;
        }

        var width = this.canvasWidth - this.margin.left - this.margin.right,
            height = this.canvasHeight - this.margin.top - this.margin.bottom;

        var x_translate = 110;
        var y_translate = 130;

        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickFormat(function (d) {
                return "Area " + d;
            });

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            // HOW TO DISPLAY VALUES ON Y AXIS
            .tickFormat(function (d) {
                if (d >= 10000)
                    return (d / 1000).toFixed(0) + "K";
                if (d >= 1000)
                    return (d / 1000).toFixed(1) + "K";
                return d;
            });

        svg.attr("width", width + this.margin.left + this.margin.right)
            .attr("height", height + this.margin.top + this.margin.bottom)
            .append("g")
            .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

        var x_values = [];
        tempData.forEach(function (d, i) {
            if (d.ID == Math.floor(i / 6) && String(selected[Math.floor(i / 6)]) == "true" && d.GROUP == race)
                x_values.push(d.ID);
        });
        x.domain(x_values);

        var values_to_display = [];
        y.domain([0, d3.max(tempData, function (d, i) {
            if (d.ID == Math.floor(i / 6) && String(selected[Math.floor(i / 6)]) == "true" && d.GROUP == race) {
                switch (age) {
                case "all":
                    {
                        if (gender == "people") return (parseInt(d.MTOT) + parseInt(d.FTOT)) / areas[d.ID];
                        if (gender == "males") return (parseInt(d.MTOT)) / areas[d.ID];
                        if (gender == "females") return (parseInt(d.FTOT)) / areas[d.ID];
                    }
                    break;
                case "under18":
                    {
                        if (gender == "people") return (parseInt(d.MU18) + parseInt(d.FU18)) / areas[d.ID];
                        if (gender == "males") return (parseInt(d.MU18)) / areas[d.ID];
                        if (gender == "females") return (parseInt(d.FU18)) / areas[d.ID];
                    }
                    break;
                case "under30":
                    {
                        if (gender == "people") return (parseInt(d.MU30) + parseInt(d.FU30)) / areas[d.ID];
                        if (gender == "males") return (parseInt(d.MU30)) / areas[d.ID];
                        if (gender == "females") return (parseInt(d.FU30)) / areas[d.ID];
                    }
                    break;
                case "under70":
                    {
                        if (gender == "people") return (parseInt(d.MU70) + parseInt(d.FU70)) / areas[d.ID];
                        if (gender == "males") return (parseInt(d.MU70)) / areas[d.ID];
                        if (gender == "females") return (parseInt(d.FU70)) / areas[d.ID];
                    }
                    break;
                case "over70":
                    {
                        if (gender == "people") return (parseInt(d.MO70) + parseInt(d.FO70)) / areas[d.ID];
                        if (gender == "males") return (parseInt(d.MO70)) / areas[d.ID];
                        if (gender == "females") return (parseInt(d.FO70)) / areas[d.ID];
                    }
                    break;

                }
            }
        })]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(" + x_translate + "," + (height + y_translate + 5) + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .attr("transform", "translate(" + (x_translate) + "," + y_translate + ")")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 10)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Number / sq mi");

        tempData.forEach(function (d, i) {
            if (d.ID == Math.floor(i / 6) && String(selected[Math.floor(i / 6)]) == "true" && d.GROUP == race) {

                var val;
                switch (age) {
                case "all":
                    {
                        if (gender == "people") val = (parseInt(d.MTOT) + parseInt(d.FTOT)) / areas[d.ID];
                        if (gender == "males") val = (parseInt(d.MTOT)) / areas[d.ID];
                        if (gender == "females") val = (parseInt(d.FTOT)) / areas[d.ID];
                    }
                    break;
                case "under18":
                    {
                        if (gender == "people") val = (parseInt(d.MU18) + parseInt(d.FU18)) / areas[d.ID];
                        if (gender == "males") val = (parseInt(d.MU18)) / areas[d.ID];
                        if (gender == "females") val = (parseInt(d.FU18) / areas[d.ID]);
                    }
                    break;
                case "under30":
                    {
                        if (gender == "people") val = (parseInt(d.MU30) + parseInt(d.FU30)) / areas[d.ID];
                        if (gender == "males") val = (parseInt(d.MU30)) / areas[d.ID];
                        if (gender == "females") val = (parseInt(d.FU30)) / areas[d.ID];
                    }
                    break;
                case "under70":
                    {
                        if (gender == "people") val = (parseInt(d.MU70) + parseInt(d.FU70)) / areas[d.ID];
                        if (gender == "males") val = (parseInt(d.MU70)) / areas[d.ID];
                        if (gender == "females") val = (parseInt(d.FU70)) / areas[d.ID];
                    }
                    break;
                case "over70":
                    {
                        if (gender == "people") val = (parseInt(d.MO70) + parseInt(d.FO70)) / areas[d.ID];
                        if (gender == "males") val = (parseInt(d.MO70)) / areas[d.ID];
                        if (gender == "females") val = (parseInt(d.FO70)) / areas[d.ID];
                    }
                    break;

                }

                svg.append("rect")
                    .attr("class", "bar")
                    .attr("fill", color)
                    .attr("transform", "translate(" + x_translate + "," + y_translate + ")")
                    .attr("x", x(d.ID))
                    .attr("width", x.rangeBand())
                    .attr("y", y(val))
                    .attr("height", height - y(val));
            }
        });
    },


    updateData: function (gender, age, selected) {
        this.gender = gender;
        this.age = age;
        this.selected = selected;
        var fileToLoad = "data/age-gender.csv";
        d3.csv(fileToLoad, this.inDataCallbackFunc);
    },


    /////////////////////////////////////////////////////////////


    updateScreen: function () {
        this.updateWindow();
        this.updateData(this.gender, this.area, "dont_edit");
    },

    setRace: function (race) {
        this.race = race;
        this.updateData(this.gender, this.age, this.selected);
    },

    setSelected: function (selected) {
        this.selected = selected;
        this.updateData(this.gender, this.age, this.selected);
    },

    setAge: function (age) {
        this.age = age;
    },

    setGender: function (gender) {
        this.gender = gender;
    },

    setAreas: function (areas) {
        this.areas = areas;
    }

});


//////////////////////////////////////////////////////////////
// OTHER
/////////////////////////////////////////////////////////////