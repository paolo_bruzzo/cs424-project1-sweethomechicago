function Class() {}

Class.prototype.construct = function () {};

Class.extend = function (def) {
    var classDef = function () {
        if (arguments[0] !== Class) {
            this.construct.apply(this, arguments);
        }
    };

    var proto = new this(Class);
    var superClass = this.prototype;

    for (var n in def) {
        var item = def[n];
        if (item instanceof Function) item.superClass = superClass;
        proto[n] = item;
    }

    classDef.prototype = proto;

    //Give this new class the same static extend method    
    classDef.extend = this.extend;
    return classDef;
};

////////////////////////////////////////

// code adapted from http://bl.ocks.org/mbostock/3202354

var piechart_App = Class.extend({

    construct: function () {

        this.scaleFactor = 1;

        this.gender = "people";
        this.age = "all";
        this.area = 0;

        this.margin = {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
        };
        this.vizWidth = 0;
        this.vizHeight = 0;

        this.canvasWidth = 250;
        this.canvasHeight = 500;

        this.svg = null;
        this.myTag = "";
    },

    /////////////////////////////////////////////////////////////

    initApp: function () {
        this.inDataCallbackFunc = this.inDataCallback.bind(this);
    },

    /*
initAppWithRoomandYear: function(roomNumber, yearNumber)
{
  this.room = +roomNumber; // number
  this.year = yearNumber.toString(); // string
  this.inDataCallbackFunc = this.inDataCallback.bind(this);
},
*/

    ////////////////////////////////////////

    startup: function (whereToRender) {
        this.myTag = whereToRender;
        this.svg = d3.select(this.myTag).append("svg");

        this.updateScreen();
    },

    /////////////////////////////////////////////////////////////

    updateWindow: function () {
        var xWin, yWin;

        xWin = d3.select(this.myTag).style("width");
        yWin = d3.select(this.myTag).style("height");

        this.vizWidth = xWin;
        this.vizHeight = yWin;

        var totalSizeX = this.canvasWidth + this.margin.left + this.margin.right;
        var totalSizeY = this.canvasHeight + this.margin.top + this.margin.bottom;

        this.svg = d3.select(this.myTag).select("svg")
            .attr("width", this.vizWidth)
            .attr("height", this.vizHeight)
            .attr("viewBox", "0 0 " + totalSizeX + " " + this.canvasHeight)
        //.attr("viewBox", "" + -this.margin.left + " 0 " + totalSizeX + " " + this.canvasHeight)
        .attr("preserveAspectRatio", "xMinYMin meet");

        //this.svg = d3.select("#viz").select("svg").select("g")
        //.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
    },


    /////////////////////////////////////////////////////////////

    inDataCallback: function (tempData) {
        //Create SVG element
        this.svg.selectAll("g").remove();

        var gender = this.gender;
        var age = this.age;
        var area = this.area;

        var svg = this.svg;
        var w = this.canvasWidth;
        var outerRadius = w / 2;
        var innerRadius = w / 8;
        var arc = d3.svg.arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius);

        var pie = d3.layout.pie();
        //Easy colors accessible via a 10-step ordinal scale
        var color = ["#dff5ff", "#120505", "#cea437", "#9f2200", "#005ca7"];

        var dataset = [];
        var i = 0;
        var total;

        tempData.forEach(function (d) {
            if (d.ID == area && d.GROUP == "Total")
                switch (gender) {
                case "people":
                    {
                        if (age == "all")
                            total = parseInt(d.MTOT) + parseInt(d.FTOT);
                        if (age == "under18")
                            total = parseInt(d.MU18) + parseInt(d.FU18);
                        if (age == "under30")
                            total = parseInt(d.MU30) + parseInt(d.FU30);
                        if (age == "under70")
                            total = parseInt(d.MU70) + parseInt(d.FU70);
                        if (age == "over70")
                            total = parseInt(d.MO70) + parseInt(d.FO70);
                    }
                    break;
                case "males":
                    {
                        if (age == "all")
                            total = parseInt(d.MTOT);
                        if (age == "under18")
                            total = parseInt(d.MU18);
                        if (age == "under30")
                            total = parseInt(d.MU30);
                        if (age == "under70")
                            total = parseInt(d.MU70);
                        if (age == "over70")
                            total = parseInt(d.MO70);
                    }
                    break;
                case "females":
                    {
                        if (age == "all")
                            total = parseInt(d.FTOT);
                        if (age == "under18")
                            total = parseInt(d.FU18);
                        if (age == "under30")
                            total = parseInt(d.FU30);
                        if (age == "under70")
                            total = parseInt(d.FU70);
                        if (age == "over70")
                            total = parseInt(d.FO70);
                    }
                    break;
                }
        });

        tempData.forEach(function (d) {
            if (d.ID == area && d.GROUP != "Total") {
                var value;
                switch (gender) {
                case "people":
                    {
                        if (age == "all")
                            value = parseInt(d.MTOT) + parseInt(d.FTOT);
                        if (age == "under18")
                            value = parseInt(d.MU18) + parseInt(d.FU18);
                        if (age == "under30")
                            value = parseInt(d.MU30) + parseInt(d.FU30);
                        if (age == "under70")
                            value = parseInt(d.MU70) + parseInt(d.FU70);
                        if (age == "over70")
                            value = parseInt(d.MO70) + parseInt(d.FO70);
                    }
                    break;
                case "males":
                    {
                        if (age == "all")
                            value = parseInt(d.MTOT);
                        if (age == "under18")
                            value = parseInt(d.MU18);
                        if (age == "under30")
                            value = parseInt(d.MU30);
                        if (age == "under70")
                            value = parseInt(d.MU70);
                        if (age == "over70")
                            value = parseInt(d.MO70);
                    }
                    break;
                case "females":
                    {
                        if (age == "all")
                            value = parseInt(d.FTOT);
                        if (age == "under18")
                            value = parseInt(d.FU18);
                        if (age == "under30")
                            value = parseInt(d.FU30);
                        if (age == "under70")
                            value = parseInt(d.FU70);
                        if (age == "over70")
                            value = parseInt(d.FO70);
                    }
                    break;
                }
                value = value / total * 100;
                dataset[i] = value.toFixed(4);
                i++;
            }
        });


        //Set up groups
        var arcs = svg.selectAll("g.arc")
            .data(pie(dataset))
            .enter()
            .append("g")
            .attr("class", "arc")
            .attr("transform", "translate(" + (outerRadius + 15) + "," + (outerRadius + 100) + ")");

        //Draw arc paths
        arcs.append("path")
            .attr("fill", function (d, i) {
                return color[i];
            })
            .attr("stroke", "#c9c9c9")
            .attr("d", arc)
            .each(function (d) {
                this._current = d;
            });


        //Labels
        arcs.append("text")
            .attr("transform", function (d) {
                return "translate(" + arc.centroid(d) + ")";
            })
            .attr("text-anchor", "middle")
            .attr("fill", function (d, i) {
                if (i == 0)
                    return "black";
                return "white";
            })
            .text(function (d) {
                if (d.value >= 3) return d.value.toFixed(0) + "%";
            });
    },


    updateData: function (gender, age, area) {

        this.gender = gender;
        this.age = age;
        this.area = area;
        var fileToLoad = "data/age-gender.csv";
        d3.csv(fileToLoad, this.inDataCallbackFunc);
    },


    /////////////////////////////////////////////////////////////


    updateScreen: function () {
        this.updateWindow();
        this.updateData(this.gender, this.area);
    },

});


//////////////////////////////////////////////////////////////
// OTHER
/////////////////////////////////////////////////////////////