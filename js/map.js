function Class() {}

Class.prototype.construct = function () {};

Class.extend = function (def) {
    var classDef = function () {
        if (arguments[0] !== Class) {
            this.construct.apply(this, arguments);
        }
    };

    var proto = new this(Class);
    var superClass = this.prototype;

    for (var n in def) {
        var item = def[n];
        if (item instanceof Function) item.superClass = superClass;
        proto[n] = item;
    }

    classDef.prototype = proto;

    //Give this new class the same static extend method    
    classDef.extend = this.extend;
    return classDef;
};

////////////////////////////////////////

// code adapted from http://bl.ocks.org/mbostock/3202354

var map_App = Class.extend({

    construct: function () {

        this.scaleFactor = 1;

        this.gender = "people";
        this.age = "all";
        this.selected = {};
        for (i = 1; i <= 77; i++)
            this.selected[i] = false;

        this.margin = {
            top: 10,
            right: 10,
            bottom: 10,
            left: 10
        };
        this.vizWidth = 0;
        this.vizHeight = 0;

        this.canvasWidth = 353;
        this.canvasHeight = 379;

        this.svg = null;
        this.myTag = "";
    },

    /////////////////////////////////////////////////////////////

    initApp: function (barchart_App1) {
        this.inDataCallbackFunc = this.inDataCallback.bind(this);

        var piechart_App1;
        piechart_App1 = new piechart_App();
        piechart_App1.initApp();
        piechart_App1.startup("#piechart_viz");

        this.piechart_App1 = piechart_App1;
        this.barchart_App1 = barchart_App1;
    },

    /*
    initAppWithRoomandYear: function(roomNumber, yearNumber)
    {
      this.room = +roomNumber; // number
      this.year = yearNumber.toString(); // string
      this.inDataCallbackFunc = this.inDataCallback.bind(this);
    },
    */

    ////////////////////////////////////////

    startup: function (whereToRender) {
        this.myTag = whereToRender;
        this.svg = d3.select(this.myTag).append("svg");

        this.updateScreen();

    },

    /////////////////////////////////////////////////////////////

    updateWindow: function () {
        var xWin, yWin;

        xWin = d3.select(this.myTag).style("width");
        yWin = d3.select(this.myTag).style("height");

        this.vizWidth = xWin;
        this.vizHeight = yWin;

        var totalSizeX = this.canvasWidth + this.margin.left + this.margin.right;
        var totalSizeY = this.canvasHeight + this.margin.top + this.margin.bottom;

        this.svg = d3.select(this.myTag).select("svg")
            .attr("width", this.vizWidth)
            .attr("height", this.vizHeight)
            .attr("viewBox", "0 0 " + totalSizeX + " " + this.canvasHeight)
        //.attr("viewBox", "" + -this.margin.left + " 0 " + totalSizeX + " " + this.canvasHeight)
        .attr("preserveAspectRatio", "xMinYMin meet");

        //this.svg = d3.select("#viz").select("svg").select("g")
        //.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    },


    /////////////////////////////////////////////////////////////

    inDataCallback: function (tempData) {
        var piechart_App1 = this.piechart_App1;
        var barchart_App1 = this.barchart_App1;
        var svg = this.svg;
        var gender = this.gender;
        var age = this.age;
        var valuesToDisplay = {};
        var lowcolor = "#ceffce";
        var highcolor = "#046d18";

        // NB: the values to display will be updated later to "population density"
        switch (this.gender) {
        case "people":
            tempData.forEach(function (d) {
                if (d.GROUP == "Total") {
                    if (age == "all")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MTOT) + parseInt(d.FTOT);
                    if (age == "under18")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU18) + parseInt(d.FU18);
                    if (age == "under30")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU30) + parseInt(d.FU30);
                    if (age == "under70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU70) + parseInt(d.FU70);
                    if (age == "over70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MO70) + parseInt(d.FO70);
                }
            });
            break;
        case "males":
            tempData.forEach(function (d) {
                if (d.GROUP == "Total") {
                    if (age == "all")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MTOT);
                    if (age == "under18")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU18);
                    if (age == "under30")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU30);
                    if (age == "under70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MU70);
                    if (age == "over70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.MO70);
                }
            });
            break;
        case "females":
            tempData.forEach(function (d) {
                if (d.GROUP == "Total") {
                    if (age == "all")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.FTOT);
                    if (age == "under18")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.FU18);
                    if (age == "under30")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.FU30);
                    if (age == "under70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.FU70);
                    if (age == "over70")
                        valuesToDisplay[parseInt(d.ID)] = parseInt(d.FO70);
                }
            });
            break;
        default:
            console.log("Error");
        }

        // Check browser type
        var off_x = 90;
        if (window.navigator.userAgent.indexOf("Chrome") != -1) {
            off_x = 90;
        } else {
            off_x = 200;
        }

        // clear out the canvas
        this.svg.selectAll("*").remove();
        d3.select("#legend").selectAll("*").remove();
        var dropdown = d3.select("#areas-dropdown");
        dropdown.selectAll("*").remove();
        dropdown.append("option").attr("disabled", "").text("Areas");

        infobox_init(tempData, this.gender, this.age);
        piechart_App1.updateData(gender, age, 0);

        var selected = this.selected;
        for (i = 1; i <= 77; i++)
            selected[i] = false;

        //Load in GeoJSON data
        d3.json("data/map.json", function (json) {
            var center = d3.geo.centroid(json);
            var offset = [off_x, 200];
            var projection = d3.geo.mercator().scale(40000).center(center).translate(offset);
            var path = d3.geo.path().projection(projection);

            var areas = [231.61];
            // Update values to density
            json.features.forEach(function (d) {
                valuesToDisplay[d.id] = valuesToDisplay[d.id] / parseFloat(d.properties.area);
                areas.push(d.properties.area);
                dropdown.append("option")
                    .attr("value", d.id)
                    .text("Area " + d.id + ": " + d.properties.name);
            })
            barchart_App1.setAreas(areas);

            // Color Range
            var colorMap = d3.scale.linear()
                .domain([0, max(valuesToDisplay)])
                .range([lowcolor, highcolor]);

            placeLegend(lowcolor, highcolor, gender, age, 0, max(valuesToDisplay));

            var lastSelected = 0;


            //Bind data and create one path per GeoJSON feature
            svg.selectAll("path")
                .data(json.features)
                .enter()
                .append("path")
                .attr("id", function (d) {
                    return "area" + d.id;
                })
                .attr("d", path)
                .attr("fill", function (d) {
                    return colorMap(valuesToDisplay[d.id]);
                })
                .on("click", function (d) {

                    var centroid = path.centroid(d);
                    var cx = centroid[0];
                    var cy = centroid[1];

                    //If the area is not selected
                    if (!selected[d.id]) {

                        // Color all the others of orange and remove the stroke
                        for (i = 1; i <= 77; i++)
                            if (selected[i] == true)
                                svg.select("#area" + i).attr("fill", "#ebbf02")
                                .style("stroke", "none"); //.style("stroke-width","1");                      

                            // Color this one with yellow
                        d3.select(this)
                            .transition().duration(200)
                            .attr("fill", "#ebbf02")
                            .style("stroke", "rgba(0, 0, 0, 0.50)").style("stroke-width", "5");

                        svg.append("text")
                            .attr("id", "text" + d.id)
                            .attr("x", cx - 6)
                            .attr("y", cy + 3)
                            .text(d.id)
                            .attr("font-size", "12");

                        selected[d.id] = true;
                        map_App1.setSelected(selected);
                        lastSelected = d.id;

                        infobox_display(tempData, d, gender, age);
                        piechart_App1.updateData(gender, age, d.id);
                    } else {
                        // If the area is already selected, and it's the current selection
                        if (lastSelected == d.id) {
                            // Take it back to normal    
                            d3.select(this)
                                .transition().duration(200)
                                .attr("fill", function (d) {
                                    return colorMap(valuesToDisplay[d.id]);
                                })
                                .style("stroke", "none"); //.style("stroke-width","3");
                            svg.select("#text" + d.id).remove();

                            selected[d.id] = false;
                            map_App1.setSelected(selected);
                            lastSelected = 0;

                            infobox_init(tempData, gender, age);
                            piechart_App1.updateData(gender, age, 0);
                        }
                        // If the area is already selected, but it is NOT the current selection
                        else {
                            // Color all the others by orange
                            for (i = 1; i <= 77; i++)
                                if (selected[i] == true)
                                    svg.select("#area" + i).attr("fill", "#ebbf02")
                                    .style("stroke", "none"); //.style("stroke-width","1");
                                // Color this one by yellow
                            d3.select(this)
                                .transition().duration(200)
                                .attr("fill", "#ebbf02")
                                .style("stroke", "rgba(0, 0, 0, 0.50)"); //.style("stroke-width","1");

                            lastSelected = d.id;

                            infobox_display(tempData, d, gender, age);
                            piechart_App1.updateData(gender, age, d.id);
                        }
                    }
                })
        });
    },


    updateData: function (gender) {

        this.gender = gender;
        this.barchart_App1.setSelected([]);
        this.barchart_App1.setGender(this.gender);
        var fileToLoad = "data/age-gender.csv";
        d3.csv(fileToLoad, this.inDataCallbackFunc);

    },

    updateAge: function (age) {

        this.age = age;
        this.barchart_App1.setSelected([]);
        this.barchart_App1.setAge(this.age);
        var fileToLoad = "data/age-gender.csv";
        d3.csv(fileToLoad, this.inDataCallbackFunc);

    },


    /////////////////////////////////////////////////////////////


    updateScreen: function () {
        this.updateWindow();
        this.updateData(this.gender);
    },

    setSelected: function (selected) {
        this.selected = selected;
        this.barchart_App1.setSelected(selected);
    },

    getSelected: function () {
        return this.selected;
    },

    blinkArea: function (area_id) {

        var area = d3.select("#area" + area_id);
        var old_color = area.attr("fill");

        area.transition().duration(1000)
            .attr("fill", "#d80000");

        area.transition().delay(3200).duration(1000)
            .attr("fill", old_color);

    },

    blinkDistrict: function (distric_id) {

        var districts = [
                ["Far North Side", 1, 2, 3, 4, 9, 10, 11, 12, 13, 14, 76, 77],
                ["Northwest Side", 15, 16, 17, 18, 19, 20],
                ["North Side", 5, 6, 7, 21, 22],
                ["Central Chicago", 8, 32, 33],
                ["West Side", 23, 24, 25, 26, 27, 28, 29, 30, 31],
                ["Southwest Side", 56, 57, 58, 59, 61, 62, 63, 64, 65, 66, 67, 68],
                ["South Side", 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 60, 69],
                ["Far Southwest Side", 70, 71, 72, 73, 74, 75],
                ["Far Southeast Side", 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55]
            ];

        var distr = districts[distric_id - 1];
        for (var i = 1; i < distr.length; i++)
            this.blinkArea(distr[i]);
    }

}); // End of class


//////////////////////////////////////////////////////////////
// OTHER
/////////////////////////////////////////////////////////////

function max(array) {
    var max = 0;
    for (i = 1; i <= 77; i++)
        if (array[i] > max)
            max = array[i];
    return max;
}

// Fires when the user selects an area on the map
function infobox_display(csv, map, gender, age) {
    var color = ["#dff5ff", "#120505", "#cea437", "#9f2200", "#005ca7"];
    d3.selectAll("table").remove();
    var table = d3.select("#infotable")
        .append("table");

    var titleLine = table.append("tr");
    var col1 = titleLine.append("td").text("Races in Area " + map.id + ": " + map.properties.name);
    var col2 = titleLine.append("td").text("People / sq mi");
    var col3 = titleLine.append("td").text("Col");

    var i = 0;

    csv.forEach(function (t) {
        if (parseInt(t.ID) == map.id) {
            var newLine = table.append("tr");
            newLine.append("td").text(t.GROUP);
            var density
            switch (gender) {
            case "people":
                {
                    if (age == "all")
                        density = (parseInt(t.MTOT) + parseInt(t.FTOT));
                    if (age == "under18")
                        density = (parseInt(t.MU18) + parseInt(t.FU18));
                    if (age == "under30")
                        density = (parseInt(t.MU30) + parseInt(t.FU30));
                    if (age == "under70")
                        density = (parseInt(t.MU70) + parseInt(t.FU70));
                    if (age == "over70")
                        density = (parseInt(t.MO70) + parseInt(t.FO70));
                }
                break;
            case "males":
                {
                    if (age == "all")
                        density = parseInt(t.MTOT);
                    if (age == "under18")
                        density = parseInt(t.MU18);
                    if (age == "under30")
                        density = parseInt(t.MU30);
                    if (age == "under70")
                        density = parseInt(t.MU70);
                    if (age == "over70")
                        density = parseInt(t.MO70);
                }
                break;
            case "females":
                {
                    if (age == "all")
                        density = parseInt(t.FTOT);
                    if (age == "under18")
                        density = parseInt(t.FU18);
                    if (age == "under30")
                        density = parseInt(t.FU30);
                    if (age == "under70")
                        density = parseInt(t.FU70);
                    if (age == "over70")
                        density = parseInt(t.FO70);
                }
                break;
            }
            density = density / parseFloat(map.properties.area);
            newLine.append("td").text(dotSeparator(density.toFixed(0)))
                .style("text-align", "right");
            newLine.append("td").attr("bgcolor", color[i]);
            i++;
        }
    });
}

// Fires at the beginning
function infobox_init(csv, gender, age) {
    var color = ["#dff5ff", "#120505", "#cea437", "#9f2200", "#005ca7"];
    d3.selectAll("table").remove();
    var table = d3.select("#infotable")
        .append("table");

    var titleLine = table.append("tr");
    var col1 = titleLine.append("td").text("Races in Chicago");
    var col2 = titleLine.append("td").text("People / sq mi");
    var col3 = titleLine.append("td").text("Col");

    var chicagoArea = 231.61; //sqared miles
    var i = 0;

    csv.forEach(function (t) {
        if (parseInt(t.ID) == 0) {
            var newLine = table.append("tr");
            newLine.append("td").text(t.GROUP);
            var density
            switch (gender) {
            case "people":
                {
                    if (age == "all")
                        density = (parseInt(t.MTOT) + parseInt(t.FTOT));
                    if (age == "under18")
                        density = (parseInt(t.MU18) + parseInt(t.FU18));
                    if (age == "under30")
                        density = (parseInt(t.MU30) + parseInt(t.FU30));
                    if (age == "under70")
                        density = (parseInt(t.MU70) + parseInt(t.FU70));
                    if (age == "over70")
                        density = (parseInt(t.MO70) + parseInt(t.FO70));
                }
                break;
            case "males":
                {
                    if (age == "all")
                        density = parseInt(t.MTOT);
                    if (age == "under18")
                        density = parseInt(t.MU18);
                    if (age == "under30")
                        density = parseInt(t.MU30);
                    if (age == "under70")
                        density = parseInt(t.MU70);
                    if (age == "over70")
                        density = parseInt(t.MO70);
                }
                break;
            case "females":
                {
                    if (age == "all")
                        density = parseInt(t.FTOT);
                    if (age == "under18")
                        density = parseInt(t.FU18);
                    if (age == "under30")
                        density = parseInt(t.FU30);
                    if (age == "under70")
                        density = parseInt(t.FU70);
                    if (age == "over70")
                        density = parseInt(t.FO70);
                }
                break;
            }
            density = density / chicagoArea;
            newLine.append("td").text(dotSeparator(density.toFixed(0)))
                .style("text-align", "right");
            newLine.append("td").attr("bgcolor", color[i]);
            i++;
        }
    });
}

function dotSeparator(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

function placeLegend(lowcolor, highcolor, gender, age, min, max) {
    var legendsvg =
        d3.select("#legend")
        .append("svg")
        .attr("width", "100%").attr("height", "100%")
        .attr("viewBox", "0 0 504 67")
        .attr("preserveAspectRatio", "xMinYMin meet");

    var gradient = legendsvg.append("defs")
        .append("linearGradient")
        .attr("id", "grad1")
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "100%")
        .attr("y2", "0%");

    gradient.append("stop")
        .attr("offset", "0%").attr("style", "stop-color:" + lowcolor + ";stop-opacity:1");
    gradient.append("stop")
        .attr("offset", "100%").attr("style", "stop-color:" + highcolor + ";stop-opacity:1");

    legendsvg.append("rect")
        .attr("id", "leg")
        .attr("x", "25%")
        .attr("y", "50%")
        .attr("width", "45%").attr("height", "30%")
        .attr("fill", "url(#grad1)");

    var agetext;
    switch (age) {
    case "all":
        agetext = "";
        break;
    case "under18":
        agetext = "under 18";
        break;
    case "under30":
        agetext = "19-30";
        break;
    case "under70":
        agetext = "31-70";
        break;
    case "over70":
        agetext = "over 70";
        break;
    }

    legendsvg.append("text")
        .attr("x", "27%")
        .attr("y", "75%")
        .text(gender + " " + agetext + " / sq mi");

    legendsvg.append("text")
        .attr("x", "20%")
        .attr("y", "75%")
        .attr("fill", "white")
        .text(min);

    legendsvg.append("text")
        .attr("x", "73%")
        .attr("y", "75%")
        .attr("fill", "white")
        .text(dotSeparator(max.toFixed(0)));
}